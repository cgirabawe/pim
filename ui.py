

from PyQt4 import QtGui, QtCore
from pydc1394 import DC1394Library, Camera
from threading import Thread
from datetime import datetime as dt
import time, sys, os, cv2, threading

import numpy as np

class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow,self).__init__()
        self.resize(700,540)
        self.move(300,300)
        self.setWindowTitle("Camera Acquisition")
        self.scene = QtGui.QGraphicsScene(self)
        self.view = QtGui.QGraphicsView(self.scene)
        self.view.setSizeIncrement(2,2)
        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.view)
        self.setLayout(self.vbox)

        #initiate camera
        l = DC1394Library()
        try:
            list_cameras = l.enumerate_cameras()
        except:
            list_cameras = []

        if len(list_cameras) > 0:
            print len(list_cameras), "camera(s) found:"
            for i,c in enumerate(list_cameras):
                print i,"\t",c['guid']
        else:
            print "No camera was found. Program exiting ..."
            sys.exit()
        self._cam = Camera(l,guid = l.enumerate_cameras()[0]['guid'])

        #start camera 
        self._cam.start(interactive=True)
        print "camera started"
        time.sleep(1)

        self.timer = QtCore.QTimer()
        self.timer.setInterval(1)
        self.timer.timeout.connect(self.displayImage)
        self.timer.start()

        
        self.show()

    def displayImage(self):
        if self._cam.running:
            frame = self._cam.current_image
            timestamp = frame.timestamp
            now_time = str(dt.now())
            filename = now_time.replace(':','').replace('.','_').replace(' ','_')
            print now_time,"\t",timestamp,'\t',threading.current_thread().name
            img = np.asarray(frame[:,:])
            h,w = img.shape
            pix = QtGui.QPixmap(QtGui.QImage(img.tostring(),w,h,QtGui.QImage.Format_RGB888).rgbSwapped())
            self.scene.clear()
            self.scene.addPixmap(pix)
        else:
            print "camera was stopped and now starting ..."
            self._cam.start(interactive=True)
            time.sleep(1)

    def closeEvent(self,event):
        self._cam.stop()
        print "camera stopped"
        self._cam.close()
        print "camera closed"
        self.timer.stop()
        


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainWindow = MainWindow()
    sys.exit(app.exec_())